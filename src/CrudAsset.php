<?php
namespace atiline\AjaxCrudAssetCustomBS4;

use yii\web\AssetBundle;

/**
 * Class CrudAsset
 * @package common\assets
 */
class CrudAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = [
        'ajaxcrud.css'
    ];

    public $js = [
        'ModalRemote.js',
        'ajaxcrud.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
